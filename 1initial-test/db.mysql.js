/**
 * Backend MySQL database file
 *
 * @author Invatechs Software https://www.invatechs.com
 * @namespace DB
 */

/* eslint-disable */

'use strict';

var options = require('../options');
var mysql = require('mysql');
var sqlBuilder = require('squel');
var moment = require('moment');
var db = null;

/**
 * DB connection starts after init()
 *
 * @function init
 * @memberof DB
 */
exports.init = function() {
  return new Promise(function(resolve, reject) {
    db = mysql.createConnection(options.db.url);
    db.connect(function(e) {
      if (!e) {
        resolve();
      } else {
        reject();
        console.log('Error connecting to MySQL database: ' + e);
      }
    });
  });
};

/**
 * DB create device
 *
 * @function init
 * @memberof DB
 * @param {object} deviceData
 * @returns {Promise}
 */
var createDevice = function(deviceData) {
  return new Promise(function(resolve, reject) {

    let sql = sqlBuilder.insert();
    sql
      .into('devices')
      .set('lat', deviceData.location.lat)
      .set('lng', deviceData.location.lng)
      .set('createDate', deviceData.createDate.toString());

    db.query(sql.toString(), function(err, result) {
      if(!err) {
        resolve(Number(result.insertId));
      } else {
        console.log('Error creating new device: ' + err);
        reject(err);
      }
    });

  });
};
exports.createDevice = createDevice;

/**
 * DB add media to device
 *
 * @function init
 * @memberof DB
 *
 * @param {string} deviceId
 * @param {object} mediaData
 * @returns {Promise}
 *
 */
var attachMedia = function(deviceId, mediaData) {
  return new Promise(function(resolve, reject) {

    let out = [];

    mediaData.forEach(e => {
      out.push({
        deviceId: Number(deviceId),
        mediaId: Number(e.id),
        campaignStart: Math.floor(e.campaignStart.getTime()/1000),
        campaignEnd: Math.floor(e.campaignEnd.getTime()/1000),
        timeStart: e.timeStart,
        timeEnd: e.timeEnd
      });
    });


    let sql = sqlBuilder.insert();
    sql
      .into('`media-on-devices`')
      .setFieldsRows(out);

console.log(sql.toString());
    db.query(sql.toString(), function(err, result) {
      if(!err) {
        resolve(Number(result.insertId));
      } else {
        console.log('Error creating new device: ' + err);
        reject(err);
      }
    });

  });
};
exports.attachMedia = attachMedia;

/**
 * DB returns all devices ids
 *
 * @function init
 * @memberof DB
 *
 * @returns {Promise}
 *
 */
var getDevices = function() {
  return new Promise(function(resolve, reject) {

    var sql = sqlBuilder.select();
    sql
      .fields(['deviceId as _id'])
      .from('devices');

    db.query(sql.toString(), function(err, result) {
      if(!err) {
        resolve(result);
      } else {
        console.log('Error loading all devices ids: ' + err);
        reject(err);
      }
    });

  });
};
exports.getDevices = getDevices;

/**
 * DB find suitable devices by the search request
 *
 * @function init
 * @memberof DB
 *
 * @param {string} projectionRequest
 * @returns {Promise}
 *
 */
var findDevices = function(projectionRequest) {
  return new Promise(function(resolve, reject) {

//    var sql = sqlBuilder.select();


    //SELECT * FROM `kino-mo`.`devices` AS D
    //LEFT JOIN (SELECT * FROM `kino-mo`.`media-on-devices` WHERE campaignStart < 1464987600 AND campaignEnd > 1462050000 AND timeStart < 21 AND timeEnd > 18) AS M1
    //ON D.id=M1.deviceId;


    //sql
    //  .from('devices as D')
    //  .left_join(
    //    sqlBuilder
    //      .select()
    //      .fields(['deviceId as dId', 'mediaId', 'campaignStart', 'campaignEnd', 'timeStart', 'timeEnd'])
    //      .from('`media-on-devices`')
    //      .where('campaignStart < ' + Math.floor(projectionRequest.campaignEnd.getTime()/1000))
    //      .where('campaignEnd > ' + Math.floor(projectionRequest.campaignStart.getTime()/1000))
    //      .where('timeStart < ' + projectionRequest.timeEnd)
    //      .where('timeEnd > ' + projectionRequest.timeStart),
    //    'M',
    //    'D.deviceId = M.dId'
    //  );

    let sql = 'SELECT * FROM (SELECT D.deviceId, D.lat, D.lng, D.createDate, M1.mediaId, M1.campaignStart, M1.campaignEnd, M1.timeStart, M1.timeEnd FROM `kino-mo`.`devices` AS D ' +
    'LEFT JOIN `kino-mo`.`media-on-devices` AS M1 ' +
    'ON D.deviceId=M1.deviceId ' +
    'WHERE M1.campaignStart < ' + Math.floor(projectionRequest.campaignEnd.getTime()/1000) + ' AND M1.campaignEnd > ' + Math.floor(projectionRequest.campaignStart.getTime()/1000) + ' AND M1.timeStart < ' + projectionRequest.timeEnd + ' AND M1.timeEnd > ' + projectionRequest.timeStart + ') AS A ' +
    'UNION ' +
    'SELECT D.deviceId, D.lat, D.lng, D.createDate, \'mediaId\'=null, \'campaignStart\'=null, \'campaignEnd\'=null, \'timeStart\'=null, \'timeEnd\'=null FROM `kino-mo`.`devices` AS D ' +
    'WHERE D.deviceId NOT IN (SELECT D.deviceId FROM `kino-mo`.`devices` AS D ' +
    'LEFT JOIN `kino-mo`.`media-on-devices` AS M1 ' +
    'ON D.deviceId=M1.deviceId ' +
    'WHERE M1.campaignStart < ' + Math.floor(projectionRequest.campaignEnd.getTime()/1000) + ' AND M1.campaignEnd > ' + Math.floor(projectionRequest.campaignStart.getTime()/1000) + ' AND M1.timeStart < ' + projectionRequest.timeEnd + ' AND M1.timeEnd > ' + projectionRequest.timeStart + ');';



    db.query(sql, function(err, result) {
      if(!err) {
        let out = [];

        //result.forEach(e => {
        //  if(!out[e.deviceId]) {
        //    out[e.deviceId] = {
        //      location: {
        //        lat: e.lat,
        //        lng: e.lng
        //      },
        //      createDate: e.createDate
        //    };
        //    out[e.deviceId].media = [];
        //  }
        //  if(e.dId !== null) {
        //    out[e.deviceId].media.push({
        //      campaignStart: new Date(e.campaignStart * 1000),
        //      campaignEnd: new Date(e.campaignEnd * 1000),
        //      timeStart: e.timeStart,
        //      timeEnd: e.timeEnd
        //    });
        //  }
        //});

        resolve(result);
      } else {
        console.log('Error loading all devices ids: ' + err);
        reject(err);
      }
    });

  });
};
exports.findDevices = findDevices;