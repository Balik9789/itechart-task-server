/* eslint-disable */

'use strict';

var db = require('./db.mongo.js');
//var db = require('db.mysql.js');
//var options = require('../options');
var chance = new require('chance')();
var moment = require('moment');

beforeEach(function (done) {
  setTimeout(done, 1000);
});



describe('How to run empty test', function() {

  it('Should just be ok', function(done) {

    done();

  });

});


describe('How database works', function() {

  it('Should init database', function (done) {

    db.init()
      .then(function() {
        done();
      })
      .catch(function(e) {
        console.log(e);
      });

  });

  // it('Should add 100 devices', function (done) {
  //
  //  var arr = [];
  //
  //  for(let i = 0; i < 1000; i++) {
  //    let newDevice = {
  //      createDate: new Date(),
  //      location: {
  //        lat: chance.latitude(),
  //        lng: chance.longitude()
  //      },
  //      media: []
  //    };
  //    arr.push(db.createDevice(newDevice));
  //  }
  //
  //  Promise
  //    .all(arr)
  //    .then(function(result) {
  //      console.log(result);
  //      done();
  //    })
  //    .catch(function(e) {
  //      console.log(e);
  //    });
  //
  // });

  // it('Should clear mediaTimeline', (done) => {
  //
  //   db.getDeviceCollection().updateMany({}, {$set: {mediaTimeline: []}}, (err, data) => {
  //     if(err) {
  //       console.log(err);
  //       return;
  //     }
  //
  //     done();
  //   });
  // });

  it('Should populate devices with media', (done) => {

    var randomDate = function(start, end) {
      return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    };

    // var setRandomCampaignDate = function() {
    //   let date = randomDate(new Date(2016, 4, 1), new Date(2017, 0, 1));
    //   this.periodStart = moment(new Date(date)).hour(0).minute(0).second(0).millisecond(0).toDate();
    //   this.periodEnd = moment(new Date(date)).hour(23).minute(59).second(59).millisecond(999).toDate();
    // };

   var setRandomMediaSlots = function() {

     let date = randomDate(new Date(2016, 8, 1), new Date(2016, 12, 31));

     let start = chance.integer({min: 0, max: 23});
     let end = chance.integer({min: start + 1, max: 24});

     this.start = moment(new Date(date)).hour(start).minute(0).second(0).millisecond(0).toDate();

     this.end = moment(new Date(date)).hour(end).minute(0).second(0).millisecond(0).toDate();

   };

   db
     .getDevices()
     .then(function(devicesList) {

       let arr = [];

       devicesList.forEach(function(e) {

         console.log(e._id);

         let mediaArray = [];
         let iterations = chance.integer({min: 50, max: 70});
         let campaignId = chance.string({pool: '123456789abcdef', length: 24});
         let mediaId = [];

         for(let i = 0; i < 4; i++) {
           mediaId.push(chance.string({pool: '123456789abcdef', length: 24}));
         }


         for(let i = 0; i < iterations; i++) {
           let media = {
             mediaId: mediaId[chance.integer({min: 0, max: 3})],
             campaignId: campaignId
           };

           //setRandomCampaignDate.call(media);
           setRandomMediaSlots.call(media);
           mediaArray.push(media);
         }

         arr.push(db.attachMedia(e._id, mediaArray));

       });

       Promise
         .all(arr)
         .then(function() {
           done();
         })
         .catch(function(e) {
           console.log(e);
         });

     })
     .catch(function(e) {
       console.log(e);
     });

  });

  // it('Should read available slots from the database', function(done) {
  //
  //  let searchRequest = {
  //    campaignStart: new Date(2016, 4, 1),
  //    campaignEnd: new Date(2016, 5, 4),
  //    timeStart: 18,
  //    timeEnd: 21
  //  };
  //  db
  //    .findDevices(searchRequest)
  //    .then(function(result) {
  //
  //      result.forEach(e => {
  //        if(e.media) {
  //          console.log(e.media);
  //        } else {
  //          console.log([]);
  //        }
  //      });
  //
  //      done();
  //    })
  //    .catch(function(e) {
  //      console.log(e);
  //    });
  // });

  // it('Should emulate database load while reading available slots', function(done) {
  //
  //
  //   let arr = [];
  //   let start = new Date().getTime();
  //   for(let i = 0; i < 100; i++) {
  //
  //     let searchRequest = {
  //       campaignStart: new Date(2016, 4, 1),
  //       campaignEnd: new Date(2016, 5, 4),
  //       timeStart: 18,
  //       timeEnd: 21
  //     };
  //     arr.push(db.findDevices(searchRequest));
  //   }
  //
  //
  //   Promise.all(arr)
  //     .then(function(result) {
  //       let stop = new Date().getTime();
  //       console.log('test time' + stop - start + 'ms');
  //       done();
  //     })
  //     .catch(function(e) {
  //       console.log(e);
  //     });
  // });

});