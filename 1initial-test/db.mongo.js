/**
 * Backend MongoDB database file
 *
 * @author Invatechs Software https://www.invatechs.com
 * @namespace DB
 */

/* eslint-disable */

'use strict';

var options = require('../options');
var ObjectID = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;

var db = null;


/**
 * DB connection starts after init()
 *
 * @function init
 * @memberof DB
 */
exports.init = function() {
  return new Promise(function(resolve, reject) {

    MongoClient.connect(options.db.url, {auto_reconnect: true}, function(e, d) {
      if(!e) {
        db = d;
        resolve();
      } else {
        reject('Mongo connecting error: ' + e);
      }
    });

  });
};

/**
 * DB create device
 *
 * @function init
 * @memberof DB
 * @param {object} deviceData
 * @returns {Promise}
 */
var createDevice = function(deviceData) {
  return new Promise(function(resolve, reject) {

    db.collection('devices').insertOne(deviceData, function(err, result) {
      if(!err) {
        resolve(result.insertedId);
      } else {
        console.log('Error creating new device: ' + err);
        reject(err);
      }
    });

  });
};
exports.createDevice = createDevice;

/**
 * DB add media to device
 *
 * @function init
 * @memberof DB
 *
 * @param {string} deviceId
 * @param {object} mediaData
 * @returns {Promise}
 *
 */
var attachMedia = function(deviceId, mediaData) {
  return new Promise(function(resolve, reject) {

    let query = {};

    try {
      query._id = deviceId;
    } catch(e) {
      console.log('Wrong device id provided');
      reject(e);
    }

    if(!Array.isArray(mediaData)) {
      mediaData = [].push(mediaData);
    }

    db.collection('devices').updateOne(query, {$push: {mediaTimeline: {$each: mediaData}}}, function(err, result) {
      if(!err) {
        resolve();
      } else {
        console.log('Error creating new device: ' + err);
        reject(err);
      }
    });

  });
};
exports.attachMedia = attachMedia;

/**
 * DB returns all devices ids
 *
 * @function init
 * @memberof DB
 *
 * @returns {Promise}
 *
 */
var getDevices = function() {
  return new Promise(function(resolve, reject) {

    db.collection('devices').find({}, {_id: 1}).toArray(function(err, result) {
      if(!err) {
        resolve(result);
      } else {
        console.log('Error loading all devices ids: ' + err);
        reject(err);
      }
    });

  });
};
exports.getDevices = getDevices;

/**
 * DB find suitable devices by the search request
 *
 * @function init
 * @memberof DB
 *
 * @param {string} projectionRequest
 * @returns {Promise}
 *
 */
var findDevices = function(projectionRequest) {
  return new Promise(function(resolve, reject) {

    let query = {};
    let projection = {
      media: {
        $elemMatch: {
          campaignEnd: {$gt: projectionRequest.campaignStart},
          campaignStart: {$lt: projectionRequest.campaignEnd},
          timeStart: {$lt: projectionRequest.timeEnd},
          timeEnd: {$gt: projectionRequest.timeStart}
        }
      }
    };

    db.collection('devices').find(query, projection).toArray(function(err, result) {
      if(!err) {
        resolve(result);
      } else {
        console.log('Error creating new device: ' + err);
        reject(err);
      }
    });

  });
};
exports.findDevices = findDevices;

var getDeviceCollection = function() {
  return db.collection('devices');
};
exports.getDeviceCollection = getDeviceCollection;



// db.getCollection('devices').aggregate([
//   {
//     $match: {}
//   },
//   {$project: {
//     mediaTimeline: {$filter: {
//       input: '$mediaTimeline',
//       as: 'media',
//       cond: {$and: [
//         {$gte: ['$$media.periodStart', ISODate('2016-10-28T21:00:00.000Z')]},
//         {$lte: ['$$media.periodEnd', ISODate('2016-11-29T20:00:00.000Z')]},
//         {$gte: ['$$media.timeStart', 14]},
//         {$lte: ['$$media.timeEnd', 18]},
//       ]}
//     }},
//     _id: '$_id'
//   }},
//   {$unwind: '$mediaTimeline'},
//
//
//   //{$unwind: '$mediaTimeline'},
//   //{$sort: {'mediaTimeline.periodStart': 1}},
// ])


// db.getCollection('devices').aggregate([
//   {
//     $match: {}
//   },
//   {$project: {
//     mediaTimeline: {$filter: {
//       input: '$mediaTimeline',
//       as: 'media',
//       cond: {$or: [{
//         $and: [
//           {$gte: ['$$media.timeStart', ISODate('2016-10-28T21:00:00.000Z')]},
//           {$lte: ['$$media.timeEnd', ISODate('2016-11-29T20:00:00.000Z')]},
//         ]
//       },
//         {$and: [
//           {$gte: ['$$media.timeStart', ISODate('2016-07-28T21:00:00.000Z')]},
//           {$lte: ['$$media.timeEnd', ISODate('2016-08-29T20:00:00.000Z')]},
//         ]
//         }
//       ]}
//     }},
//     _id: '$_id'
//   }},
//   {$unwind: '$mediaTimeline'},
//   {$group: {
//     _id: {day: {$dayOfYear: "$mediaTimeline.timeStart"}, hour: {$hour: "$mediaTimeline.timeStart"}},
//     items: {$push: {mediaTimeline: '$mediaTimeline', _id: '$_id'}}
//   }},
//   {$sort: {'_id.day': 1, '_id.hour': 1}},
//
//
//
// ])