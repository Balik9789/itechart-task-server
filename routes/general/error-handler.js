/**
 * KM Error handler middleware
 *
 * @author Invatechs Software https://www.invatechs.com
 */

'use strict';

const ApiResp = require('../../utils/api-resp');

/**
 * Handles error from express
 *
 * @param {object} err
 * @param {object} request
 * @param {object} response
 * @param {object} next
 */
module.exports = (err, request, response, next) => {
  console.log(err);
  let message = 'Internal Server Error';
  let status = 500;
  if(err.status) {
    status = err.status;
    switch(err.status) {
      case 400:
        message = 'Bad request';
        break;
      case 401:
        message = 'Unauthorized access';
        break;
      case 404:
        message = 'Page not found';
        break;
      case 500:
        message = 'Internal Server Error';
        break;
      case 502:
        message = 'Bad Gateway';
        break;
      case 504:
        message = 'Gateway Timeout';
        break;
      default:
        message = 'Wrong request data. Checkout your input';
        break;
    }
  }

  let out = new ApiResp(status, message);
  response.status(out.status).send(out);
};
