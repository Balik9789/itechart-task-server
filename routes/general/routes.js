const userRoutes = require('./../user/user-routes');
const adminUsersRoutes = require('./../admin/user-routes');
const resourceRoutes = require('./../user/resources-routes');
const userSessionRoutes = require('./../user/session-routes');
const adminSessionRoutes = require('./../admin/session-routes');
const sessionCheck = require('./token-reader');
const express = require('express');
const app = express();
const router = express.Router();

router.use('*', sessionCheck)
router.use('/user/0/user', userRoutes);
router.use('/user/0/resource', resourceRoutes);
router.use('/user/0/session', userSessionRoutes);
router.use('/admin/0/session', adminSessionRoutes);
router.use('/admin/0/user', adminUsersRoutes);
module.exports = router;