/**
 * Token reader route
 *
 * @namespace Utility Routes: Token Reader
 */

'use strict';

const express = require('express');
const Session = require('../../model/session')
const router = express.Router();
const ApiResp = require('../../utils/api-resp');



/**
 * Read token
 *
 * @function ALL
 * @memberof Utility Routes: Token Reader
 * @param {string} url - /api/*
 *
 * @param {object} request
 * @param {object} request.headers
 * @param {string} request.headers.km-auth
 * @param {string} request.headers.km-client-mode
 * @param {string} request.headers.km-client-id
 * @param {object} request.cookies
 * @param {string} request.cookies.km-token
 * @param {string} request.originalUrl
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {object} response.message
 */
router.all('*', function(request, response, next) {
  let sessionToken = request.headers['auth'];
  if(!sessionToken) {
    sessionToken = null;
  }

  if((!request.baseUrl.match('session') && !(request.baseUrl.match('/user/0/user') && (request.method === 'POST' || request.baseUrl.match('activate')))) || request.baseUrl.match('/user/0/session/update')) {
    if(!sessionToken) {
      let out = new ApiResp(401, 'Unauthorized access');
      response.status(out.status).send(out);
    } else {
      let type = 'user'
      if(request.baseUrl.match('admin')) {
        type = 'admin'
      }
      request.sessionToken = sessionToken;
      let session = new Session(sessionToken, type);
      session
        .load()
        .then((res) => {
          request.user = res.user;
          next();
        }).catch((err) => {
          let out
          if(err.name === "SessionError") {
            out = new ApiResp(401, 'Unauthorized access');
          } else {
            out = new ApiResp(500, 'Something went wrong, try again later.');
          }
          response.status(out.status).send(out);
        });
    }
  } else {
    next();
  }

});

module.exports = router;