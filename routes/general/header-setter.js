/**
 * KM Content type header setter
 *
 * @author Invatechs Software https://www.invatechs.com
 */

'use strict';

const express = require('express');

const router = express.Router();


router.all('*', function(request, response, next) {
  console.log(request)
  response.type('application/json');

  next();

});

module.exports = router;