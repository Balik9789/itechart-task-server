/**
 *  Resources Routes API
 *
 * @namespace API Resources Routes: Resources
 */

'use strict';

const options = require('../../options');
const express = require('express');
const ApiResp = require('../../utils/api-resp');
const User = require('../../model/user');
const Resource = require('../../model/resource');
const Session = require('../../model/session');
const Chance = require('chance');
const chance = new Chance();
const DB = require('../../db/db').DB;
const escape = require('../../utils/str-escape');

const router = express.Router();

/**
 * Buy resources
 *
 * @function Post
 * @memberof API Resources Routes: Resources
 * @param {string} url - /api/user/0/resource/buy
 *
 * @param {object} request
 * @param {object} request.body
 *
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
router.post('/buy', function(request, response) {
  let user = new User(request.user);
  let resource = new Resource(request.body.resource._id);
  let count = request.body.count;
  let price;
    user.load()
    .then(() => {
      return resource.load()
    })
    .then(() => {
      price = resource.getCurrentPrice();
      if(!user.resources[resource._id]) {
        user.resources[resource._id] = {};
      }
      user.resources[resource._id].name =  resource.name;
      if(user.resources[resource._id].count) {
        user.resources[resource._id].count = user.resources[resource._id].count + count;
      } else {
        user.resources[resource._id].count = count;
      }
      user.balance = user.balance - price * count;
      user.operations.push({
        resourceId: resource._id,
        totalPrice: price * count,
        count: count,
        action: "buy",
        date: new Date()
      });
      return user.update();
    }).then(() => {
      let out = new ApiResp(200, 'OK');
      response.status(out.status).send(out);
    }).catch((err) => {
      console.log(err)
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    })
});

 /**
 * Sell resources
 *
 * @function Post
 * @memberof API Resources Routes: Resources
 * @param {string} url - /api/user/0/resource/sell
 *
 * @param {object} request
 * @param {object} request.body
 *
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
router.post('/sell', function(request, response) {
  let user = new User(request.user);
  let resource = new Resource(request.body.resource._id);
  let count = request.body.count;
  let price;
  resource
    .load()
    .then(() => {
      price = resource.getCurrentPrice();
      if(!user.resources[resource._id]) {
        user.resources[resource._id] = {};
      }
      user.resources[resource._id].name =  resource.name;
      if(user.resources[resource._id].count) {
        user.resources[resource._id].count = user.resources[resource._id].count - count;
      } else {
        user.resources[resource._id].count = -count;
      }
      user.balance = user.balance + price * count;
      user.operations.push({
        resourceId: resource._id,
        totalPrice: price * count,
        count: count,
        action: "sell",
        date: new Date()
      });
      return user.update();
    }).then(() => {
      let out = new ApiResp(200, 'OK');
      response.status(out.status).send(out);
    }).catch((err) => {
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    })
});


module.exports = router;