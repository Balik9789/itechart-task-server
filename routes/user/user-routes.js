/**
 *  User Routes API
 *
 * @namespace API User Routes: User
 */

'use strict';

const options = require('../../options');
const express = require('express');
const ApiResp = require('../../utils/api-resp');
const User = require('../../model/user');
const Session = require('../../model/session');
const Chance = require('chance');
const chance = new Chance();
const DB = require('../../db/db').DB;
const escape = require('../../utils/str-escape');

const router = express.Router();


/**
 * Create user
 *
 * @function Post
 * @memberof API User Routes: User
 * @param {string} url - /api/user/0/user
 *
 * @param {object} request
 * @param {object} request.body
 * @param {string} request.body.username
 * @param {string} request.body.password
 * @param {string} request.body.firstName
 * @param {string} request.body.lastName
 * @param {string} request.body.state
 * @param {number} request.body.clientId
 * @param {string} request.body.description
 * @param {string[]} request.body.permissionList
 * @param {string} request.body.userRoleId
 * @param {string} request.body.position
 * @param {string} request.body.avatar
 * @param {string} request.body.nickname
 * @param {string} request.body.locale
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.post('', function(request, response) {

   let newUser = new User(request.body)
   if(!User.checkUserData(newUser)) {
     let out = new ApiResp(400, 'Unsupported params');
     return response.status(out.status).send(out);
   }
   let user = new User(newUser);
   let session;

   user
    .save()
    .then((res) => {
      let sessionCred = {
        username: newUser.username,
        password: newUser.password
      }
      session = new Session(sessionCred, 'user');
      return session.create();
    })
    .then((usr) => {
      let message = {
        user: usr,
        token: session.sessionToken
      }
      let out = new ApiResp(200, message);
      response.status(out.status).send(out);
    })
    .catch((err) => {
      console.log(err)
       let out = null;
       if(err.name === 'UserError') {
         out = new ApiResp(400, 'User exist');
       } else {
         if(err.code === 11000) {
           out = new ApiResp(400, 'Username already exists');
         } else {
           out = new ApiResp(500, 'Something went wrong, try again later.');
         }
       }
       response.status(out.status).send(out);
    });

 });


 /**
 * Fill up user balance
 *
 * @function PUT
 * @memberof API User Routes: User
 * @param {string} url - /api/user/0/user/fillup
 *
 * @param {object} request
 * @param {object} request.body
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.put('/fillup', function(request, response) {

  let user = new User(request.user._id);
  let count = request.body.count;
  user
    .load()
    .then(() => {
      console.log(user)
      user.balance = user.balance + count;
      console.log(user.balance)
      return user.update()
    }).then(() => {
      let out = new ApiResp(200, 'OK');
      response.status(out.status).send(out);
    }).catch((err) => {
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    });
 });


 /**
 * Activate user
 *
 * @function GET
 * @memberof API User Routes: User
 * @param {string} url - /api/user/0/user/fillup
 *
 * @param {object} request
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.get('/activate/:activationCode/user/:userId', function(request, response) {
  console.log(request.params);
  let user = new User(request.params.userId);
  let activationCode = request.params.activationCode;
  user
    .load()
    .then(() => {
      if(user.activationCode === activationCode) {
        user.activationCode = '';
        return user.update()
      } else {
        let out = new ApiResp(200, 'User is not activated');
        response.status(out.status).send(out);
      }
    }).then(() => {
      let out = new ApiResp(200, 'User successful activate');
      response.status(out.status).send(out);
    }).catch((err) => {
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    });
 });


module.exports = router;