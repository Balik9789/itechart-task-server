/**
 * Session Routes API
 *
 * @namespace API Session Routes: Session
 */

'use strict';

const options = require('../../options');
const express = require('express');
const ApiResp = require('../../utils/api-resp');
const Session = require('../../model/session');

const router = express.Router();

/**
 * Create session
 *
 * @function Post
 * @memberof API Session Routes: Session
 * @param {string} url - /api/user/0/session
 *
 * @param {object} request
 * @param {object} request.body
 * @param {string} request.body.username
 * @param {string} request.body.password
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.post('', function(request, response) {
  let sessionCred = request.body
  let session = new Session(sessionCred, 'admin');
  session
    .create()
    .then((user) => {
      let message = {
        user: user,
        token: session.sessionToken
      }
      let out = new ApiResp(200, message);
      response.status(out.status).send(out);
    })
    .catch((err) => {
      console.log(err)
       let out = null;
      out = new ApiResp(500, 'Something went wrong, try again later.');
       response.status(out.status).send(out);
    });

 });


 /**
 * Delete session
 *
 * @function Delete
 * @memberof API Session Routes: Session
 * @param {string} url - /api/user/0/session
 *
 * @param {object} request
 * @param {object} request.body
 * @param {string} request.body.username
 * @param {string} request.body.password
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.delete('', function(request, response) {
 let sessionCred = request.headers.auth
 let session = new Session(sessionCred, 'admin');
  session
    .delete()
    .then(() => {
      let out = new ApiResp(200, "OK");
      response.status(out.status).send(out);
    })
    .catch((err) => {
        let out = new ApiResp(500, 'Something went wrong, try again later.');
       response.status(out.status).send(out);
    });

 });


 /**
 * Update session
 *
 * @function Update
 * @memberof API Session Routes: Session
 * @param {string} url - /api/user/0/session/update
 *
 * @param {object} request
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.get('/update', function(request, response) {
  let sessionCred = request.headers.auth
  let session = new Session(sessionCred, 'admin');
  Session
    .updateSessionById(request.user._id)
    .then(() => {
      return session.load()
    })
    .then((res) => {
      let message = {
        user: res.user,
        token: session.sessionToken
      }
      let out = new ApiResp(200, message);
      response.status(out.status).send(out);

    })
    .catch((err) => {
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    });

 });

 module.exports = router