/**
 *  User Routes API
 *
 * @namespace API User Routes: User
 */

'use strict';

const options = require('../../options');
const express = require('express');
const ApiResp = require('../../utils/api-resp');
const User = require('../../model/user');
const Session = require('../../model/session');
const Chance = require('chance');
const chance = new Chance();
const DB = require('../../db/db').DB;
const escape = require('../../utils/str-escape');

const router = express.Router();


/**
 * Create user
 *
 * @function Post
 * @memberof API User Routes: User
 * @param {string} url - /api/admin/0/user
 *
 * @param {object} request
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.get('', function(request, response) {
  User
    .getUsers({})
    .then((res) => {
      let out = new ApiResp(200, res);
      response.status(out.status).send(out);
    }).catch((err) => {
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    })
 });


 /**
 * Lock/unlock user
 *
 * @function GET
 * @memberof API User Routes: User
 * @param {string} url - /api/admin/0/user/lock
 *
 * @param {object} request
 * @param {object} request.body
 *
 * @param {object} response
 * @param {boolean} response.error
 * @param {number} response.status
 * @param {string} response.message
 */
 router.get('/lock/:userId', function(request, response) {

  let user = new User(request.params.userId);
  user
    .lock()
    .then(() => {
      let out = new ApiResp(200, 'OK');
      response.status(out.status).send(out);
    }).catch((err) => {
      let out = new ApiResp(500, 'Something went wrong, try again later.');
      response.status(out.status).send(out);
    })
 });


module.exports = router;