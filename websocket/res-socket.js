const Resource = require('../model/resource');
const User = require('../model/user')

module.exports = (client) => {
  let getResInterval, getActivitiesInterval;
  client.on("text", (str) => {
      getResources(client);
      //getActivities(client);

      getResInterval = setInterval(() => {
        getResources(client);
      }, 3000);

      getActivitiesInterval = setInterval(() => {
        getActivities(client);
      }, 3000);

  });


  client.on("close", (code, reason) => {
      clearInterval(getResInterval);
      clearInterval(getActivitiesInterval);
  });

}

let getResources = (client) => {
  Resource
    .getResources({})
    .then((res) => {
      let resForResponse = [];
      res.forEach((resource) => {
        resForResponse.push(new Resource(resource).getDataObject(true));
      });
      let message = {
        type: 'resources',
        resources: resForResponse
      }
      client.sendText(JSON.stringify(message));
    }).catch((err) => {
      console.log(err);
    });
}


let getActivities = (client) => {
  User
    .getLastActivities(20)
    .then((activities) => {
      let message = {
        type: 'activities',
        activities: activities
      }
      client.sendText(JSON.stringify(message));
    }).catch((err) => {
      console.log(err);
    });
}