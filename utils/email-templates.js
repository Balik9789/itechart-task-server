/**
 * KM utils email templates resolver
 *
 * @author Invatechs Software https://www.invatechs.com
 * @namespace Utils
 */
'use strict';

/**
 * Resolves email, fills template with data
 *
 * @param data
 * @param template
 */
module.exports = function(data, template) {

  let result = '';
  if(template && Object.keys(data).length) {
    result = template;
    Object.keys(data).forEach((key) => {
      result = result.replace(new RegExp(`<%= ${key} %>`, 'g'), data[key]);
    });
  }


  return result;
};
