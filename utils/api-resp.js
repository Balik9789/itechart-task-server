/**
 * KM backend API response helper class
 *
 * @author Invatechs Software https://www.invatechs.com
 * @namespace Utils
 */
'use strict';

class ApiResp {

  constructor(status, message) {

    if(typeof status !== 'number') {
      throw new Error('Api response status should be number');
    }
    if(typeof status === 'undefined') {
      throw new Error('Api response status is missing');
    }

    if(typeof message === 'undefined') {
      throw new Error('Api response message is missing');
    }

    if(status > 399) {
      this.error = true;
    } else {
      this.error = false;
    }


    this.status = status;
    this.message = message;

  }

}

module.exports = ApiResp;