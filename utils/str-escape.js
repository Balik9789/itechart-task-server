/**
 * KM utils to escape string for mongo query
 *
 * @author Invatechs Software https://www.invatechs.com
 * @namespace Utils
 */
'use strict';

module.exports = (string) => {
  let escapedStr = string;

  escapedStr = escapedStr.replace(/\\/g, '&#92;');
  escapedStr = escapedStr.replace(/\./g, '\\.');
  escapedStr = escapedStr.replace(/\//g, '\\/');
  escapedStr = escapedStr.replace(/\*/g, '\\*');
  escapedStr = escapedStr.replace(/"/g, '\\"');
  escapedStr = escapedStr.replace(/\[/g, '\\[');
  escapedStr = escapedStr.replace(/]/g, '\\]');
  escapedStr = escapedStr.replace(/\+/g, '\\+');
  escapedStr = escapedStr.replace(/\(/g, '\\(');
  escapedStr = escapedStr.replace(/\)/g, '\\)');
  escapedStr = escapedStr.replace(/\{/g, '\\{');
  escapedStr = escapedStr.replace(/}/g, '\\}');
  escapedStr = escapedStr.replace(/\?/g, '\\?');
  escapedStr = escapedStr.replace(/\|/g, '\\|');
  escapedStr = escapedStr.replace(/\^/g, '\\^');
  escapedStr = escapedStr.replace(/\$/g, '\\$');
  
  return escapedStr;
};
