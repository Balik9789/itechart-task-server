/**
 * KM backend session store
 *
 * @author Invatechs Software https://www.invatechs.com
 * @namespace Session model
 */
'use strict';


class SessionError extends Error {

  constructor(message) {
    super(message);
    this.name = 'SessionError';
  }
}

exports.SessionError = SessionError;



class UserError extends Error {

  constructor(message) {
    super(message);
    this.name = 'UserError';
  }

}

exports.UserError = UserError;

class ResourceError extends Error {

  constructor(message) {
    super(message);
    this.name = 'ResourceError';
  }

}

exports.ResourceError = ResourceError;



class DBError extends Error {

  constructor(message) {
    super(message);
    this.name = 'DBError';
  }

}

exports.DBError = DBError;

class LogError extends Error {

  constructor(message) {
    super(message);
    this.name = 'LogError';
  }

}

exports.LogError = LogError;

