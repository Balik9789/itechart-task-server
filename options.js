
  exports.port = 1337;
  exports.socketPort = 1338;
  exports.clientPort = 5555;

  exports.url = 'http://localhost:';

  exports.session = {
    expire: 60,
    longSessionExpire: 60
  };

  exports.db = {
    url: 'mongodb://localhost:27017/itechart-task'
  };


  exports.amqp = {
    host: 'localhost',
    port: 5672,
    queue: 'log'
  };


  exports.redis = {
    url: 'redis://127.0.0.1:6379'
  };


  exports.passwordReset = {
    codeValidPeriod: 1  // In days
  };

exports.randPool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';