'use strict';

module.exports = {
  'env': {
    'es6': true
  },
  'globals': {
    'describe': true,
    'it': true,
    'beforeEach': true,
    'require': true,
    'Buffer': true,
    'console': true,
    'module': true,
    'exports': true,
    'setTimeout': true,
    'setInterval': true,
    'process': true
  },
  'ecmaVersion': 6,
  'ecmaFeatures': {
    'arrowFunctions': true,
    'binaryLiterals': true,
    'blockBindings': true,
    'classes': true,
    'forOf': true,
    'generators': true,
    'objectLiteralShorthandMethods': true,
    'objectLiteralShorthandProperties': true,
    'octalLiterals': true,
    'templateStrings': true
  },
  'rules': {
    'semi': ['error', 'always'],
    'arrow-body-style': ['error', 'always'],
    'arrow-parens': [2, 'always'],
    'arrow-spacing': [2, {'before': true, 'after': true}],
    'constructor-super': 0,
    'generator-star-spacing': 0,

    'no-constant-condition': 2,
    'no-confusing-arrow': ['error', {'allowParens': false}],

    'no-class-assign': 0,
    'no-const-assign': 0,
    'no-dupe-class-members': 0,
    'no-this-before-super': 0,
    'no-var': 2,
    'object-shorthand': 0,
    'prefer-arrow-callback': 0,
    'prefer-const': 0,
    'prefer-reflect': 0,
    'prefer-spread': 0,
    'prefer-template': 0,
    'require-yield': 0,

    'space-infix-ops': 2,
    'space-before-blocks' : 2,
    'space-before-function-paren': ['error', 'never'],
    'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
    'semi-spacing': ['error', {'before': false, 'after': true}],
    'no-whitespace-before-property': 2,
    'no-mixed-requires': 2,
    'no-unused-vars': ['error', { 'vars': 'all', 'args': 'none' }],
    'no-undef': 2
  }
};