/**
 * Main backend
 *
 * @author Valiantsin Karaneuski
 * @namespace Index
 */

'use strict';

const cluster = require('cluster');
const numCPUs = 1;// require('os').cpus().length;


/**
 * One cluster set as Master and other as Workers
 *
 * @function settingClusters
 * @memberof Index
 */
if (cluster.isMaster) {

  cluster.on('exit', function(worker, code, signal) {
    cluster.fork();
    console.log('session backend worker ' + worker.process.pid + ' died. New forked.');
  });

  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

} else if(cluster.isWorker) {
  const db = require('./db/db');
  const sessionStore = require('./session-store/store');
  const server = require('./server');
  const socketServer = require('./socket-server');
  const worker = require('./schedule-worker/schedule-worker');



  db
    .init()
    .then(() => {
      console.log('db initialized at ' + new Date());
      return sessionStore.init();
    })
    .then(() => {
      console.log('session storage initialized at ' + new Date());
      return server.init();
    })
    .then(() => {
      console.log('server initialized at ' + new Date());
      return socketServer.init();
    })
    .then(() => {
      console.log('WS server initialized at ' + new Date());
      return worker.init();

    })
    .then(() => {
      console.log('schedule worker started at ' + new Date());
    })
    .catch((e) => {
      console.log(e);
      process.exit(1);
    });
}