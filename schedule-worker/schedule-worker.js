const Resource = require('../model/resource');
const random = require('../utils/random');
/**
 * Initializes schedule worker
 *
 * @function init
 * @memberOf Server
 * @returns {Promise}
 */
exports.init = () => {
  return new Promise((resolve, reject) => {
    setInterval(() => {
        Resource
          .getResources({}, {}, true)
          .then((res) => {
            let promises = []
            res.forEach((resource) => {
              promises.push(new Resource(resource).addNewPrice(random(10, 40)));
            });
            return Promise.all(promises);
          }).then(() => {
            console.log('price updated')
          }).catch((err) => {
            console.log(err);
          });
      }, 30000);
    resolve();
  })
};