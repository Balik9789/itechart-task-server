/**
 *  DB parent class
 *
 * @author Valiantsin Karaneuski
 * @namespace DB
 */

'use strict';

const options = require('../options');
const ObjectID = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const emailVerifier = require('../utils/email-verifier');
const DBError = require('../utils/errors').DBError;

let db = null;

/**
 * DB connection starts after init()
 *
 * @function init
 * @memberof DB
 */
exports.init = function() {
  return new Promise(function(resolve, reject) {

    MongoClient.connect(options.db.url, {auto_reconnect: true}, function(e, d) {
      if(!e) {
        db = d;
        resolve();
      } else {
        console.log(e);
        reject('Mongo connecting error: ' + e);
      }
    });

  });
};


/**
 * DB parent class
 *
 * @function init
 * @memberof DB
 */
class DB {

  constructor(collectionName) {
    this.collectionName = collectionName;
  }

  static getDB() {
    return db;
  }

  save() {
    return new Promise((resolve, reject) => {

      let newItem = JSON.parse(this.toJSON());

      if(!newItem.createDate) {
        newItem.createDate = new Date();
      }

      if(!newItem._id) {
        newItem._id = (new ObjectID()).toHexString();
      }

      db
        .collection(this.collectionName)
        .insertOne(newItem, {w: 1}, (err, result) => {
        if(!err) {
          this._id = result.insertedId;
          resolve(this._id);
        } else {
          reject(err);
        }
      });

    });
  }

  /**
   * Get data object
   * @returns {{}}
   */
  getDataObject() {
    return JSON.parse(this.toJSON());
  }

  update() {
    return new Promise((resolve, reject) => {

      let query = {_id: new ObjectID(this._id).toHexString()};
      let update = {$set: this.getDataObject()};

      update.$set.updateDate = new Date();
      delete update.$set.createDate;
      delete update.$set._id;

      db
        .collection(this.collectionName)
        .updateOne(query, update, {w: 1}, (err, result) => {
          if(!err) {
            if(result.modifiedCount > 0) {
              resolve();
            } else {
              reject(new DBError('Nothing was modified'));
            }
          } else {
            reject(err);
          }
        });

    });
  }

  /**
   * Loads object from database
   *
   * @param {object} [filter] Filter used to load object. If not set then object will be loaded by its ID
   * @returns {Promise}
   */
  load(filter) {
    return new Promise((resolve, reject) => {

      let query = {};
      // Filter is set then use it as query
      if(typeof filter === 'object' && Object.keys(filter).length) {
        query = filter;
      } else {  // Use default load scenario by object ID
        query = {
          _id: new ObjectID(this._id).toHexString()
        };
      }
      db
        .collection(this.collectionName)
        .find(query)
        .toArray((err, result) => {
          if(err) {
            throw new Error(err);
          }
          if(result[0]) {
            for(let item in result[0]) {
              if(result[0].hasOwnProperty(item)) {
                if(item === 'deleted' && result[0].deleted === true) {
                  reject('fetched object has been deleted');
                } else {
                  this[item] = result[0][item];
                }
              }
            }
            resolve();
          } else {
            reject('No data loaded');
          }
        });

    });
  }

  validateEmail(email) {
    return emailVerifier(email);
  }

  toJSON() {
    throw new Error('toJSON method is not implemented');
  }

  /**
   * Adds "deleted" field check query to the filter
   *
   * @param {object?} filter Filter to get documents
   * @returns {object} Returns filter object
   */
  static addDeletedFieldCheck(filter) {

    let query = {
      $and: [
        {$or: [{deleted: {$exists: false}}, {deleted: {$ne: true}}]}
      ]
    };

    if(typeof filter === 'object') {

      if(filter.$or) {
        query.$and.push(filter);
      } else {
        Object.keys(filter).forEach((field) => {

          let subQuery = {};
          subQuery[field] = filter[field];
          query.$and.push(subQuery);

        });
      }
    }

    return query;
  }

  /**
   * Deletes instances with specified IDs
   *
   * @param {string} collection Collection to delete from
   * @param {array} ids Array of IDs to delete
   */
  static deleteBulk(collection, ids) {
    return new Promise((resolve, reject) => {
      collection = collection.toString();
      if(!collection) {
        throw new Error('@deleteBulk; Wrong collection name');
      }

      if(!ids || !Array.isArray(ids)) {
        throw new Error('@deleteBulk; Wrong IDs data');
      }

      try {
        ids.forEach((id) => {
          new ObjectID(id);
        });
      } catch(e) {
        throw new Error(e);
      }

      db
        .collection(collection)
        .updateMany({
          _id: {$in: ids}
        }, {
          $set: {
            deleted: true
          }
        }, (err) => {
          if(!err) {
            resolve();
          } else {
            reject(err);
          }
        });

    });
  }

  /**
   * Updates instances with specified IDs
   *
   * @param {string} collection Collection to delete from
   * @param {array} ids Array of IDs to update
   * @param {object} Object update
   */
  static updateMany(collection, ids, updateObj) {
    return new Promise((resolve, reject) => {
      ids.forEach((id) => {
        id = new ObjectID(id).toHexString();
      });
      let query = {_id: {$in: ids}};
      let update = {$set: updateObj};

      update.$set.updateDate = new Date();
      delete update.$set._id;

      db
        .collection(collection)
        .updateMany(query, update, (err, result) => {
          if(!err) {
            if(result.modifiedCount > 0) {
              resolve(result.modifiedCount);
            } else {
              reject(new DBError('Nothing was modified'));
            }
          } else {
            reject(err);
          }
        });

    });
  }


  /**
   * Inserts many instances
   *
   * @param {string} collection Collection to insert documents
   * @param {object[]} docs Array of dociments to be inserted
   * @param {object} options Optional. If changing of default options required
   */
  static insertMany(collection, docs, options) {
    return new Promise((resolve, reject) => {
      options = options || {w: 1};

      if(!Array.isArray(docs)) {
        return reject('Wrong input data format');
      }
      try {
        docs = docs.map((doc) => {
          let newItem = JSON.parse(doc.toJSON());
          newItem.createDate = new Date();
          newItem._id = (new ObjectID()).toHexString();
          return newItem;
        });
      } catch (err) {
        return reject('Wrong input data format');
      }

      db.collection(collection)
        .insertMany(docs, options, (err, results) => {
          if(!err) {
            resolve(results.insertedIds);
          } else {
            reject(err);
          }
        });
    });
  }
}

exports.DB = DB;
