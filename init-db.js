'use strict'
const db = require('./db/db');
const Resource = require('./model/resource');



db
  .init()
  .then(() => {
    return Resource.initDBCollection();
  })
  .then(() => {
    console.log('db collections initialize')
  })
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });