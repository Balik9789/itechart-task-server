/**
 * backend admin model
 *
 * @namespace Admin model
 */
'use strict';

const DB = require('../db/db').DB;
const ObjectID = require('mongodb').ObjectID;
const bcrypt = require('bcryptjs');
const Chance = require('chance').Chance;
const moment = require('moment');
const options = require('../options');
const UserError = require('../utils/errors').UserError;


const ADMINS_COLLECTION = 'admins';

const validProperties = new Set([
  '_id',
  'username',
  'isAdmin',
  'password',
  'createDate',
  'updateDate'
]);

class Admin extends DB {
  constructor(adminData) {
    super(ADMINS_COLLECTION);


    if(typeof adminData === 'string') {

      if(this.validateEmail(adminData)) {
        this.username = adminData;
        return;
      } else {
          try {
          if(new ObjectID(adminData)) {
            this._id = adminData;
            return;
          }
        } catch(err) {}
      }

      // Throw Error if adminData in string format is not email or not ID
      throw new UserError('@admin.constructor *wrong userData format*');

    } else if(typeof adminData === 'object') {

      Object.keys(adminData).forEach((item) => {
        if(validProperties.has(item)) {
          this[item] = adminData[item];
        } else if(item === '_id') {
          this['_id'] = adminData[item];
        }
      });

    } else {
      throw new Error('Unsupported params');
    }

  }
  static checkUserData(userData) {
    let ok = true;

    Object.keys(userData).forEach((item) => {
      if(!validProperties.has(item)) {
        ok = false;
      }
    });

    return ok;
  }

  toJSON() {

    let out = {};

    validProperties.forEach((item) => {
      if(typeof this[item] !== 'undefined') {
        out[item] = this[item];
      }
    });

    if(this._id) {
      out._id = this._id;
    }
    if(this.username) {
      out.username = this.username.toLowerCase();
    }
    delete out.password;

    return JSON.stringify(out);

  }


  load() {
    if(this._id) {
      return super.load();

    } else if(this.username) {
      return new Promise((resolve, reject) => {
        DB.getDB()
          .collection(this.collectionName)
          .find({'username': this.username})
          .toArray((err, result) => {
            if(err) {
              throw new Error(err);
            }

            if(result[0]) {
              for(let item in result[0]) {
                if(result[0].hasOwnProperty(item)) {
                  if(item === 'deleted' && result[0].deleted === true) {
                    reject('fetched object has been deleted');
                  } else {
                    this[item] = result[0][item];
                  }
                }
              }
              resolve();
            } else {
              reject('No data loaded');
            }
          });

      });
    }

  }

  save() {
    //console.log(super)
    // Create activation code for new user
    this.activationCode = new Chance().string({length: 32, pool: '0123456789abcdef'});

    if(!this.password) {
      return super.save();
    } else {
      return new Promise((resolve, reject) => {
        this
          .makePasswordHash()
          .then((hash) => {
              //return Promise.resolve();
            return super.save();
          })
          .then(() => {
            resolve();
          })
          .catch((err) => {
            reject(err);
          });

      });
    }
  }

  /**
   * Creates password hash
   */
  makePasswordHash() {
    return new Promise((resolve, reject) => {

      if(!this.password) {
        reject(new UserError('Can not create password hash, password is empty'));
        return;
      }

      bcrypt.genSalt(10, (err, salt) => {
        if(!err) {
          bcrypt.hash(this.password, salt, (err, hash) => {
            if(!err) {
              this.password = hash;
              resolve(hash);
            } else {
              reject(err);
            }
          });
        } else {
          reject(err);
        }

      });
    });
  }


  /**
   * Loads admin list based on filter
   *
   * @param {object} filter Admin filter in mongo format
   * @param {object} options Filter options
   * @returns {Promise}
   */
  static getAdmins(filter, options) {
    return new Promise((resolve, reject) => {

      let query = DB.addDeletedFieldCheck(filter);

      DB.getDB()
        .collection(ADMINS_COLLECTION)
        .find(query, options)
        .toArray((err, result) => {

          if(!err) {
            let admins = [];
            result.forEach((admin) => {
              admins.push(new Admin(admin));
            });

            resolve(admins);
          } else {
            reject(err);
          }
        });
    });
  }

  /**
   * Adds timestamp to username
   *
   * @param {Array} users Array of users to change
   */
  static makeDeletedUsername(users) {
    return new Promise((resolve, reject) => {
      if(Array.isArray(users) && users.length) {
        let promises = [];

        users.forEach((user) => {
          promises.push(new Promise((resolve, reject) => {
            let update = { $set: { username: user.username + '-' + new Date().getTime().toString()}};
            DB
              .getDB()
              .collection(ADMINS_COLLECTION)
              .updateOne({
                  _id: user._id
                },
                update,
                (err, data) => {
                  if(err) {
                    reject(err);
                    return;
                  }

                  resolve();
                }
              );
          }));
        });

        Promise
          .all(promises)
          .then(() => {
            resolve();
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve();
      }
    });
  }

  /**
   * Update user last login date
   *
   * @param {string} userId
   * @returns {Promise}
   */
  static updateLastLoginDate(id) {
    return new Promise((resolve, reject) => {
      DB.getDB()
        .collection(ADMINS_COLLECTION)
        .updateOne({_id: id}, {$set: {lastLoginDate: new Date()}}, {w: 1}, (err, result) => {
          if(err) {
            reject(err);
          }
          if(result) {
            resolve('ok');
          } else {
            reject('No data update');
          }
        });
    });
  }
}


module.exports = Admin;
