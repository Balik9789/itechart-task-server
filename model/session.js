/**
 * backend session store
 *
 * @namespace Session model
 */
'use strict';

const sessionStore = require('../session-store/store');
const crypto = require('crypto');
const ObjectID = require('mongodb').ObjectID;
const bcrypt = require('bcryptjs');
const User = require('./user');
const Admin = require('./admin');
const Chance = require('chance').Chance;
const emailVerifier = require('../utils/email-verifier');
const SessionError = require('../utils/errors').SessionError;
const UserError = require('../utils/errors').UserError;

class Session {

  /**
   *
   *
   * @function Session constructor
   * @memberof Session model
   *
   * @param {object|string} credentials
   * @param {string} credentials.username
   * @param {string} credentials.password
   *
   * @returns {object} new instance of Session
   */
  constructor(credentials, type) {

    if(!credentials) {
      throw new SessionError('Missing credentials while creating session');
    }

    this.credentials = {};

    if(typeof credentials === 'string') {
      this.sessionToken = credentials;
    } else {
      if(!credentials.password || !credentials.username) {
        throw new SessionError('Missing credentials while creating session');
      }
      this.credentials = credentials;

    }
    if(!type) {
      throw new SessionError('Missing session type');
    }
    this.type = type;

  }


  static checkCredentials(credentials) {

    if(!credentials) {
      return false;
    }

    if(typeof credentials === 'object') {

      if(!credentials.password || !credentials.username) {
        return false;
      }
      if(!emailVerifier(credentials.username)) {
        return false;
      }

    } else {

      if(typeof credentials !== 'string') {
        return false;
      }

    }
    return true;
  }
  /**
   *
   *
   * @function Session create method
   * @memberof Session model
   *
   * @returns {Promise}
   */
  create() {
    return new Promise((resolve, reject) => {

      let user = null;
      if(this.type === 'user') {
        user = new User(this.credentials.username.toLowerCase());
      }
      if(this.type === 'admin') {
        user = new Admin(this.credentials.username.toLowerCase());
      }
      if(!user) {
        throw new SessionError('Incorrect session type');
      }

      let client = null;
      user
        .load()
        .then(() => {
          if(this.type === 'admin' && !user.isAdmin) {
            throw new SessionError('Incorrect Session type');
          }
          if(this.type === 'user' && user.isAdmin) {
            throw new SessionError('Incorrect Session type');
          }


          if(user.state === 'locked') {
            reject(new UserError('USER_IS_SUSPENDED'));
            return;
          }

          bcrypt.compare(this.credentials.password, user.password, (bcryptErr, compare) => {
            if(!bcryptErr) {
              if(compare) {

                let token = crypto
                  .createHash('sha256')
                  .update(new Chance().string({length: 20}))
                  .digest('hex');

                let promise;



                  promise = new Promise((resolve, reject) => {resolve();});


                promise
                  .then(() => {
                    let data = {
                      user: JSON.parse(user.toJSON()),
                      client: (client ? JSON.parse(client.toJSON()) : '')
                    };
                    data.user.longSession = this.credentials.longSession;
                    sessionStore
                      .addSessionToken(token + user._id, JSON.stringify(data), this.credentials.longSession)
                      .then(() => {
                        this.sessionToken = token + user._id;
                        resolve(user);
                      })
                      .catch((err) => {
                        reject(err);
                      });
                  })
                  .catch((e) => {
                    reject(e);
                  });

              } else {
                reject(new SessionError('password doesn\'t match'));
              }
            } else {
              reject(bcryptErr);
            }
          });
        })
        .catch((err) => {
          console.log(err)
          reject(new SessionError('user not found'));
        });

    });
  }

  /**
   * Update sessions tokens by userId
   *
   * @returns {Promise}
   * @throws {SessionError}
   */

  static updateSessionById(userId) {
    return new Promise((resolve, reject) => {
      let user = new User(userId);
      let client = {};
      let promise;
      let tokens = [];
      user
        .load()
        .then(() => {
          return sessionStore.getSessionTokensById(userId);
        }).then((sessionTokens) => {
          tokens = sessionTokens;
          let promises = [];
          tokens.forEach((token) => {
            promises.push(sessionStore.remove(token));
          });
          return Promise.all(promises);
        })
        .then(() => {
          let data = {
            user: JSON.parse(user.toJSON()),
          };
          let promises = [];
          tokens.forEach((token) => {
            promises.push(sessionStore.set(token, JSON.stringify(data)));
          });
          return Promise.all(promises);
        }).then(() => {
          resolve();
        })
        .catch((e) => {
          reject(e);
        });
    });
  }


  /**
   * Extends session token
   *
   * @returns {Promise}
   * @throws {SessionError}
   */
  extend() {
    return new Promise((resolve, reject) => {

      if(!this.sessionToken) {
        throw new SessionError('Can not extend session without sessionToken');
      }

      sessionStore
        .extendSessionToken(this.sessionToken, this.credentials.longSession)
        .then(() => {
          resolve();
        })
        .catch((e) => {
          console.log(e);
          reject(e);
        });
    });
  }
  /**
   *
   *
   * @function Session load method
   * @memberof Session model
   *
   * @returns {Promise}
   */
  load() {
    return new Promise((resolve, reject) => {

      sessionStore
        .getSession(this.sessionToken)
        .then((result) => {

          if(result) {
            let user = null;
            if(this.type === 'user') {
              user = new User(result.user);
              if(user.isAdmin) {
                throw new SessionError('Incorrect Session type');
              }
            }
            if(this.type === 'admin') {
              user = new Admin(result.user);
              if(!user.isAdmin) {
                throw new SessionError('Incorrect Session type');
              }
            }
            if(!user) {
              throw new SessionError('Invalid Session type: ' + this.type);
            }



            let out = {
              user: user
            };

            this.credentials.longSession = result.user.longSession;

            resolve(out);
          } else {
            reject(new SessionError('session doesn\'t exist'));
          }
        })
        .catch((err) => {
          reject(err);
        });

    });
  }

  delete() {
    return new Promise((resolve, reject) => {

      sessionStore
        .remove(this.sessionToken)
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
        });

    });
  }

  /**
   * Deletes multiple session entries from Redis
   *
   * @param {Array} users Users IDs to delete session for
   * @param {string} userType User types to change: 'admin' or 'user'
   * @returns {Promise}
   */
  static deleteBulk(users, userType) {
    return new Promise((resolve, reject) => {
      if(userType !== 'admin' && userType !== 'user') {
        throw new SessionError('Wrong user type');
      }

      if(Array.isArray(users) && users.length) {
        let idsCorrect = false;
        users.forEach((id) => {
          try {
            new ObjectID(id).toHexString();
            idsCorrect = true;
          } catch(e) {
            idsCorrect = false;
            reject(e);
            return;
          }
        });

        if(idsCorrect) {
          let query = {
            action: 'login',
            'currentUser.type': userType,
            'currentUser.id': {$in: users}
          };
          let options = {
            'instance.data.sessionToken': 1
          };

          Log
            .getLog(query, options)
            .then((log) => {
              if(log.length) {
                let tokens = [];
                log.forEach((item) => {
                  tokens.push(item.instance.data.sessionToken);
                });

                if(tokens.length) {
                  sessionStore
                    .remove(tokens)
                    .then(() => {
                      resolve();
                    })
                    .catch((err) => {
                      reject(err);
                    });
                }
              }
              resolve();
            })
            .catch((e) => {
              reject(new SessionError(e));
            });
        }
      } else {
        resolve();
      }
    });
  }
}


module.exports = Session;
