/**
 * backend session store
 *
 * @namespace Resource model
 */
'use strict';


const ObjectID = require('mongodb').ObjectID;
const ResourceError = require('../utils/errors').ResourceError;
const DB = require('../db/db').DB;

const validProperties = new Set([
  '_id',
  'name',
  'price',
]);
const RESOURCES_COLLECTION = 'resource';

class Resource extends DB {

  /**
   *
   *
   * @function Resource constructor
   * @memberof Resource model
   *
   * @param {object|string} data
   * @param {string} data.name
   * @param {string} data.price
   *
   * @returns {object} new instance of Resource
   */
  constructor(data) {


    super(RESOURCES_COLLECTION);
    if(!data) {
      throw new ResourceError('Missing data while creating resource');
    }
    if(typeof data === 'object') {
      if(data.hasOwnProperty('_id') && data['_id']) {
        this['_id'] = data['_id'];
      }
      Object.keys(data).forEach((item) => {
        if(validProperties.has(item)) {
          if(item !== 'price') {
            this[item] = data[item];
          } else {
            if(Array.isArray(data.price)) {
              this.price = [];
              if(data.price.length) {
                data.price.forEach((elem) => {
                  if(typeof elem === 'object') {
                    this.price.push({
                      price: elem.price,
                      date: new Date(data.date)
                    });
                  } else {
                    throw new ResourceError('Unsupported price format');
                  }
                });
              }
            } else {
              throw new ResourceError('Unsupported price format');
            }
          }
        }
      });
    } else if (typeof data === 'string') {
      this['_id'] = data;
    }
  }


  /**
   * Get user data object
   * @returns {{}}
   */
  getDataObject(outFormat) {
    let out = {};
    validProperties.forEach((item) => {
      if(typeof this[item] !== 'undefined') {
        out[item] = this[item];
      }
    });
    if(outFormat) {
      this.price.sort((a, b) => {
        let dateA = a.date;
        let dateB = b.date;
        if (dateA < dateB) {
          return -1;
        }
        if (dateA > dateB) {
          return 1;
        }
        return 0;
      });
      out.price = this.price[this.price.length -1].price
    }
    delete out.collectionName;
    return out;
  }



  getCurrentPrice() {
    this.price.sort((a, b) => {
      let dateA = a.date;
      let dateB = b.date;
      if (dateA < dateB) {
        return -1;
      }
      if (dateA > dateB) {
        return 1;
      }
      return 0;
    });
    return this.price[this.price.length -1].price
  }
  /**
   * Transform to JSON
   */
  toJSON() {
    let out = this.getDataObject();
   // delete out.password;
    return JSON.stringify(out);
  }

  static getResources(filter, options) {
    return new Promise((resolve, reject) => {

      let query = DB.addDeletedFieldCheck(filter);
      DB.getDB()
        .collection(RESOURCES_COLLECTION)
        .find(query, options)
        .toArray((err, result) => {

          if(err) {
            throw new Error(err);
          }
          if(result.length) {

            let resources = [];
            result.forEach((resource) => {
              resources.push(new Resource(resource));
            });

            resolve(resources);
          } else {
            resolve([]);
          }
        });
    });
  }

  addNewPrice(price) {
    return new Promise((resolve, reject) => {
      this.price.push({
        price: price,
        date: new Date()
      })
      let query = {_id: new ObjectID(this._id).toHexString()};
      let update = {$set: this.getDataObject()};
      DB.getDB()
        .collection(this.collectionName)
        .updateOne(query, update, {w: 1}, (err, result) => {
          if(!err) {
            if(result.modifiedCount > 0) {
              resolve();
            } else {
              reject(new ResourceError('Nothing was modified'));
            }
          } else {
            reject(err);
          }
        });
    });
  }

  static initDBCollection() {
    return new Promise((resolve, reject) => {
      let resources = [{ "name" : "iron", "price" : [ { "price" : 11, "date" : new Date("2017-08-09T06:49:24.772Z") } ] },
      { "name" : "oil", "price" : [ { "price" : 14, "date" : new Date("2017-08-09T06:49:54.140Z") } ] },
      { "name" : "wood", "price" : [ { "price" : 19, "date" : new Date("2017-08-09T06:50:09.444Z") } ] }];
      let resToSave = [];
      let promises = [];
      resources.forEach((res) => {
        resToSave.push(new Resource(res));
      });
      resToSave.forEach((res) => {
        promises.push(res.save())
      });
      Promise.all(promises)
        .then(() => {
          resolve('ok')
        }).catch((e) => {
          reject(e);
        })
    });
  }
}


module.exports = Resource;
