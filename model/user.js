/**
 * backend user model
 *
 * @namespace User model
 */
'use strict';


const DB = require('../db/db').DB;
const ObjectID = require('mongodb').ObjectID;
const bcrypt = require('bcryptjs');
const Chance = require('chance').Chance;
const UserError = require('../utils/errors').UserError;
const options = require('../options');
const moment = require('moment');

const sender = require('sender-js');
sender.init(options.sender);
const USERS_COLLECTION = 'users';


const states = ['active' , 'locked']
const validProperties = new Set([
  '_id',
  'username',
  'password',
  'state',
  'isAdmin',
  'collectionName',
  'resources',
  'balance',
  'operations',
  'createDate',
  'updateDate',
  'activationCode'
]);

const propertiesToTrim = new Set([
  'username'
]);

class User extends DB {

  constructor(userData) {
    super(USERS_COLLECTION);
   // delete this.collectionName;
    if(typeof userData === 'string') {

      if(this.validateEmail(userData)) {
        this.username = userData.toLowerCase();
        return;
      } else {
          try {
          if(new ObjectID(userData)) {
            this._id = userData;
            return;
          }
        } catch(err) {}
      }

      // Throw Error if userData in string format is not email or not ID
      throw new UserError('@user.constructor *wrong userData format*');

    } else if(typeof userData === 'object') {
      this.setData(userData);
      if(userData.hasOwnProperty('_id') && userData['_id']) {
        this['_id'] = userData['_id'];
      }
    } else {
      throw new UserError('Unsupported params');
    }
  }

  setData(userData) {
    if(typeof userData === 'object') {
      Object.keys(userData).forEach((item) => {
        if(validProperties.has(item)) {
          if(propertiesToTrim.has(item)) {
            if(Array.isArray(userData[item])) {
              this[item] = userData[item].map((e) => {
                return typeof e === 'string' ? e.trim() : e;
              });
            } else {
              this[item] = typeof userData[item] === 'string' ? userData[item].trim() : userData[item];
            }
          } else {
            this[item] = userData[item];
          }
        }
      });
    } else {
      throw new UserError('Unsupported params');
    }
  }


  /**
   * Returns users collection name
   *
   * @returns {string} Collection name
   */
  static getCollectionName() {
    return USERS_COLLECTION;
  }


  static checkUserData(userData) {
    let ok = true;


    Object.keys(userData).forEach((item) => {
      if(!validProperties.has(item)) {
        ok = false;
      }
    });

    return ok;
  }

  /**
   * Get user data object
   * @returns {{}}
   */
  getDataObject() {
    let out = {};
    validProperties.forEach((item) => {
      if(typeof this[item] !== 'undefined') {
        out[item] = this[item];
      }
    });
    if(this._id) {
      out._id = this._id;
    }
    if(this.username) {
      out.username = this.username.toLowerCase();
    }

    delete out.collectionName;
    return out;
  }

  /**
   * Transform to JSON
   */
  toJSON() {
    let out = this.getDataObject();
   // delete out.password;
    return JSON.stringify(out);
  }

  static getLastActivities(limit) {
    return new Promise((resolve,reject) => {
      let query = [
        {$project: {operations: 1, _id: 0}},
        { $unwind : "$operations" },
        { $sort : { "operations.date": -1} },
        { $limit : limit }
      ];
      DB.getDB()
        .collection(this.getCollectionName())
        .aggregate(query)
        .toArray((err, result) => {
            if(err) {
              throw new Error(err);
            }

            if(result[0]) {
              resolve(result);
            } else {
              reject('No data loaded');
            }
        })
    })
  }

  load() {
    if(this._id) {
      return super.load();
    } else if(this.username) {
      return new Promise((resolve, reject) => {

        DB.getDB()
          .collection(this.collectionName)
          .find({'username': this.username})
          .toArray((err, result) => {
            if(err) {
              throw new Error(err);
            }

            if(result[0]) {
              Object.keys(result[0]).forEach((item) => {
                if(item === 'deleted' && result[0].deleted === true) {
                  reject('fetched object has been deleted');
                } else {
                  this[item] = result[0][item];
                }
              });

              resolve();
            } else {
              reject('No data loaded');
            }
          });

      });
    }
  }

  lock() {
    return new Promise((resolve, reject) => {
      this
        .load()
        .then(() => {
          if(this.state === 'active') {
            this.state = 'locked';
          } else if(this.state === 'locked') {
            this.state = 'active';
          }

          return super.update();
        }).then(() => {
          resolve();
        }).catch((err) => {
          reject(err)
        })
    });
  }

  /**
   * Compares user password with the transfered
   *
   * @param {string} passwordToCheck Password to compare with
   * @returns {Promise}
   */
  comparePasswords(passwordToCheck) {
    return new Promise((resolve, reject) => {
      if(!this.password || !passwordToCheck) {
        throw new UserError('User password is empty');
      }

      bcrypt.compare(passwordToCheck, this.password, (bcryptErr, compare) => {

        if(!bcryptErr) {
          if(compare) {
            resolve();

          } else {
            reject(new UserError('Passwords doesn\'t match'));
          }
        } else {
          reject(bcryptErr);
        }
      });
    });
  }

  /**
   * Creates password hash
   */
  makePasswordHash() {
    return new Promise((resolve, reject) => {

      if(!this.password) {
        reject(new UserError('Can not create password hash, password is empty'));
        return;
      }

      bcrypt.genSalt(10, (err, salt) => {
        if(!err) {
          bcrypt.hash(this.password, salt, (err, hash) => {
            if(!err) {
              this.password = hash;
              resolve('OK');
            } else {
              reject(err);
            }
          });
        } else {
          reject(err);
        }

      });
    });
  }

  save() {

    this.activationCode = new Chance().string({length: 32, pool: '0123456789abcdef'});

    if(!this.password) {
      return super.save();
    } else {
      return new Promise((resolve, reject) => {
        this
          .makePasswordHash()
          .then((hash) => {

              //return Promise.resolve();
            return super.save();
          })
          .then(() => {
            var settings = {
              gmail: {
                "username": "v.koronevski.dev.techart@gmail.com",
                "password": "Balik9789"
                }
            };

            sender.init(settings);

            var messageOptions = {
                to: this.username,
                from: "techArtTask@gmail.com",
              subject: 'fff',
              text: options.url + options.port + '/api/user/0/user/activate/' + this.activationCode + '/user/' + this._id
            };

            sender.send(messageOptions, function(err, message) {});
            resolve();
          })
          .catch((err) => {
            reject(err);
          });

      });
    }
  }

  delete() {
    if(this.username) {
      return new Promise((resolve, reject) => {
        this.username += '-' + new Date().getTime();


        DB.getDB()
          .collection(USERS_COLLECTION)
          .updateOne({
            _id: this._id
          }, {
            $set: {
              username: this.username
            }
          }, (err) => {
            if(!err) {
              super.delete();
              resolve();
            } else {
              reject(err);
            }
          });
      });
    } else {
      return super.delete();
    }
  }


  /**
   * Update user last action date
   *
   * @returns {Promise}
   */
  updateLastActionDate() {
    return new Promise((resolve, reject) => {
      if(this._id) {
        DB.getDB()
          .collection(USERS_COLLECTION)
          .updateOne({_id: this._id}, {$set: {lastActionDate: new Date()}}, {w: 1}, (err, result) => {
            if(err) {
              reject(err);
            }
            if(result) {
              resolve();
            } else {
              reject('Error updating user\'s last action date');
            }
          });
        } else {
          reject('User data has not been loaded');
        }
    });
  }




  /**
   * Adds timestamp to username
   *
   * @param {Array} users Array of users to change
   */
  static makeDeletedUsername(users) {
    return new Promise((resolve, reject) => {
      if(Array.isArray(users) && users.length) {
        let promises = [];

        users.forEach((user) => {
          promises.push(new Promise((resolve, reject) => {
            let update = { $set: { username: user.username + '-' + new Date().getTime().toString()}};
            DB
              .getDB()
              .collection(USERS_COLLECTION)
              .updateOne({
                  _id: user._id
                },
                update,
                (err, data) => {
                  if(err) {
                    reject(err);
                    return;
                  }

                  resolve();
                }
              );
          }));
        });

        Promise
          .all(promises)
          .then(() => {
            resolve();
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve();
      }
    });
  }

  /**
   * Loads user list based on filter
   *
   * @param {object} filter User filter in mongo format
   * @param {object} options Filter options
   * @returns {Promise}
   */
  static getUsers(filter, options) {
    return new Promise((resolve, reject) => {

      let query = DB.addDeletedFieldCheck(filter);
      DB.getDB()
        .collection(USERS_COLLECTION)
        .find(query, options)
        .toArray((err, result) => {

          if(err) {
            throw new Error(err);
          }
          if(result.length) {

            let users = [];
            result.forEach((user) => {
              users.push(new User(user));
            });
            resolve(users);
          } else {
            resolve([]);
          }
        });
    });
  }


  /**
   * Update user last login date
   *
   * @param {string} userId
   * @returns {Promise}
   */
  static updateLastLoginDate(id) {
    return new Promise((resolve, reject) => {
      DB.getDB()
        .collection(USERS_COLLECTION)
        .updateOne({_id: id}, {$set: {lastLoginDate: new Date()}}, {w: 1}, (err, result) => {
          if(err) {
            reject(err);
          }
          if(result) {
            resolve('ok');
          } else {
            reject('No data update');
          }
        });
    });
  }
}


module.exports = User;
