const resSocket = require("./websocket/res-socket");
const ws = require("nodejs-websocket");
const options = require('./options');
/**
 * Initializes socket server
 *
 * @function init
 * @memberOf Server
 * @returns {Promise}
 */
exports.init = () => {
  return new Promise((resolve, reject) => {
    var server = ws.createServer(resSocket).listen(options.socketPort);
    resolve();
  })
};