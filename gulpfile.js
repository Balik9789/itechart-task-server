const gulp = require('gulp'),
      eslint = require('gulp-eslint'),
      jsdoc = require("gulp-jsdoc3"),
      nodemon = require('gulp-nodemon');


gulp.task('eslint', () => {
  gulp.src(['**/*.js','!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('doc', (cb) => {
  const config = {
    "opts": {
      "destination": "./doc"
    },
  };
  gulp.src(['./routes/admin/*.js','./routes/user/*.js'], {read: false})
    .pipe(jsdoc(config,cb));
});



gulp.task('default', function() {
  nodemon({
    script: 'index.js',
    ext: 'js json',
    ignore: ['node_modules/**', 'archives/**/*'],
    env: {
      'MONGO_URL': 'localhost:27017/itechart-task',
      'HTTP_PORT': '1338',
      'ENV': 'dev'
    },
    tasks: ['eslint'] })
  .on('restart', function() {
    console.log('~~~ iTechArt server restarted ~~~');
  });
});
