/**
 *  session store
 *
 * @namespace Session Store
 */
'use strict';

const options = require('../options');
const redis = require('redis');

let redisClient = null;

/**
 * Session store starts after init()
 *
 * @function init
 * @memberof Session Store
 * @returns {Promise}
 */
const init = function() {
  return new Promise(function(resolve, reject) {

    if(process.argv[3] === 'prod' || process.argv[2] === 'prod') {
      redisClient = redis.createClient(
        {
          port: options.redis.port,
          host: options.redis.host,
          auth_pass: options.redis.password,
          tls: {
            servername: options.redis.host
          }
        }
      );
    } else {
      redisClient = redis.createClient(options.redis);
    }


    redisClient.on('ready', function() {
      redisClient.set('one', 'two');
      resolve();
    });

    redisClient.on('error', function() {
      reject(new Error('Error connecting to redis server'));
    });

  });
};
exports.init = init;

/**
 * Check if user is logged in by key
 *
 * @function getSession
 * @memberof Session Store
 * @param {string} key
 * @returns {Promise}
 */
const getSession = function(key) {
  return new Promise(function(resolve, reject) {

    redisClient.get(key, function(err, reply) {
      if(!err) {
        let out = null;
        if(reply === null) {
          resolve(out);
        } else {
          try {
            out = JSON.parse(reply);
          } catch(e) {
            reject(e);
          }
          if(out !== null) {
            resolve(out);
          }
        }
      } else {
        console.log('Error checking client session');
        reject(err);
      }
    });

  });
};
exports.getSession = getSession;

/**
 * Add session token for value by key
 *
 * @function addSessionToken
 * @memberof Session Store
 * @param {object} value
 * @param {string} key
 * @param {boolean} long Flag to make long session
 * @returns {Promise}
 */
const addSessionToken = function(key, value, long) {
  return new Promise(function(resolve, reject) {

    if(typeof key !== 'string') {
      value = JSON.stringify(value);
    }
    let expireTime = (long ? options.session.longSessionExpire : options.session.expire);
    redisClient.multi().set(key, value).expire(key, expireTime).exec(function(err, reply) {
      if(!err) {
        resolve();
      } else {
        reject('@addSessionToken; ' + err);
      }
    });
  });
};
exports.addSessionToken = addSessionToken;

/**
 * Extends session token
 *
 * @function extendSessionToken
 * @memberof Session Store
 * @param {string} key
 * @param {boolean} long Flag to make long session
 * @returns {Promise}
 */
const extendSessionToken = function(key, long) {
  return new Promise((resolve, reject) => {
    let expireTime = (long ? options.session.longSessionExpire : options.session.expire);

    redisClient.multi().expire(key, expireTime).exec((err, reply) => {
      if(!err) {
        resolve();
      } else {
        reject('@extendSessionToken; ' + err);
      }
    });
  });
};
exports.extendSessionToken = extendSessionToken;

/**
 * Logout. Remove session
 *
 * @function logout
 * @memberof Session Store
 * @param {string} key
 * @return {Promise}
 */
const remove = function(key) {
  return new Promise(function(resolve, reject) {
    redisClient.del(key, function(err, reply) {
      if(!err) {
        resolve(null);
      } else {
        reject(err);
      }
    });
  });
};
exports.remove = remove;

/**
 * @function set
 * @memberof Session Store
 * @param {string} key
 * @param {*} value
 */
const set = function(key, value) {
  return new Promise(function(resolve, reject) {
    redisClient.set(key, value, function(err, reply) {
      if(!err) {
        resolve(reply);
      } else {
        reject(err);
      }
    })
  });
};
exports.set = set;

/**
 * @function setWithExpireTime
 * @memberof Session Store
 * @param {key} key
 * @param {*} value
 * @param {number} expireTime
 */
const setWithExpireTime = function(key, value, expireTime) {
    return new Promise(function(resolve, reject) {
    redisClient.multi().set(key, value).expire(key, expireTime).exec(function(err, reply) {
      if(!err) {
        resolve(reply);
      } else {
        reject(err);
      }
    })
  });
}
exports.setWithExpireTime = setWithExpireTime;

/**
 * @function get
 * @memberof Session Store
 * @param {string} key
 */
const get = function(key) {
  return new Promise(function(resolve, reject) {
    redisClient.get(key, function(err, reply) {
      if(!err) {
        resolve(reply);
      } else {
        reject(err);
      }
    });
  });
};
exports.get = get;


/**
 * @function scan
 * @memberof Session Store
 * @param {string} substring
 */
const scan = function(substring) {
  return new Promise((resolve, reject) => {
    let cursor = '0';
    let keys = [];
    let scan = () => {
      redisClient.scan(cursor, 'MATCH', '*' + substring + '*', (err, reply) => {
        if(err) {
          return reject(err);
        }
        cursor = reply[0];
        if(cursor === '0') {
          reply[1].forEach((key) => {
            keys.push(key);
          });
          resolve(keys);
        } else {
          reply[1].forEach((key) => {
            keys.push(key);
          });
          return scan();
        }
      });
    }

    scan();
  });
}
exports.scan = scan;

const getSessionTokensById  = function(id) {
  return scan(id);
};

exports.getSessionTokensById = getSessionTokensById;