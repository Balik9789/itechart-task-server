/**
 * Backend server routes
 *
 * @author Valiantsin Varaneuski
 * @namespace Server
 */

'use strict';

const options = require('./options');
const express = require('express');
const bodyParser = require('body-parser');
const busboy = require('connect-busboy');
//const crypt = require('./utils/crypt');
const cors = require('cors');
const errorHandler = require('./routes/general/error-handler');
const routes = require('./routes/general/routes');
const app = express();
const http = require('http');


/**
 * Initializes server routes
 *
 * @function init
 * @memberOf Server
 * @returns {Promise}
 */
exports.init = () => {
  return new Promise((resolve, reject) => {

    app.options("/isAuthenticated", cors());

    app.use(cors({
      origin: options.url + options.clientPort,
      credentials: true
    }));

    app.use(busboy({
      limits: {
        fileSize: 100 * 1024 * 1024
      }
    }));

    app.use(bodyParser.json({
      limit: '50mb'
    }));

    app.use(errorHandler);
    app.use('/api', routes);
    app.listen(options.port);
    resolve();
  });
};